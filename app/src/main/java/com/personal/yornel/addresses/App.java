package com.personal.yornel.addresses;

import android.app.Application;

import com.google.firebase.auth.FirebaseAuth;
import com.personal.yornel.addresses.model.AddressRecord;

public class App extends Application {

    private FirebaseAuth mAuth;

    private AddressRecord address;

    @Override
    public void onCreate() {
        super.onCreate();
        mAuth = FirebaseAuth.getInstance();
    }

    public FirebaseAuth getAuth() {
        return mAuth;
    }

    public AddressRecord getAddress() {
        return address;
    }

    public void setAddress(AddressRecord address) {
        this.address = address;
    }
}
