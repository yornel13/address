package com.personal.yornel.addresses;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import static com.personal.yornel.addresses.SettingsActivity.closeSession;
import static com.personal.yornel.addresses.SettingsActivity.rateThis;
import static com.personal.yornel.addresses.SettingsActivity.sendFeedback;
import static com.personal.yornel.addresses.SettingsActivity.shareApp;

public class AppSettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.app_preferences);

        // feedback preference click listener
        Preference myPref = findPreference(getString(R.string.key_send_feedback));
        myPref.setOnPreferenceClickListener(preference -> {
            sendFeedback(getActivity());
            return true;
        });

        Preference rateThis = findPreference(getString(R.string.rate_this));
        rateThis.setOnPreferenceClickListener(preference -> {
            rateThis(getActivity());
            return true;
        });

        Preference shareThis = findPreference(getString(R.string.share_app));
        shareThis.setOnPreferenceClickListener(preference -> {
            shareApp(getActivity());
            return true;
        });

        Preference pref1 = findPreference(getString(R.string.key_app_version));
        try {
            pref1.setSummary(appVersion());
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        Preference closeSession = findPreference(getString(R.string.key_session));
        closeSession.setOnPreferenceClickListener(preference -> {
            closeSession(getActivity());
            return true;
        });
    }

    public String appVersion() throws PackageManager.NameNotFoundException {
        PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        return pInfo.versionName;
    }
}
