package com.personal.yornel.addresses;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.personal.yornel.addresses.util.AppPreferences;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.EventBusException;

public abstract class BaseActivity extends AppCompatActivity {

    protected App app;

    protected AppPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (App) getApplicationContext();
        preferences = new AppPreferences(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (EventBusException e) {
            // No Subscriber
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            EventBus.getDefault().unregister(this);
        } catch (EventBusException e) {
            // No Subscriber
        }
    }
}
