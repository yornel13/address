package com.personal.yornel.addresses;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.material.snackbar.Snackbar;
import com.personal.yornel.addresses.controller.AddressesController;
import com.personal.yornel.addresses.model.AddressRecord;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateAddressActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.input_title)
    EditText titleText;

    @BindView(R.id.input_description)
    EditText descriptionText;

    @BindView(R.id.main_container)
    View mainView;

    @BindView(R.id.accuracy)
    TextView accuracyText;

    private GoogleMap map;

    private LatLng currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address);
        ButterKnife.bind(this);
        setupMap();
    }

    @OnClick(R.id.close)
    public void close() {
        finish();
    }

    private void setupMap() {
        if (map == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
        map.setOnMyLocationChangeListener(location -> {
            setAccuracy(location.getAccuracy());
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate center = CameraUpdateFactory
                    .newLatLng(currentLocation);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);
            map.moveCamera(center);
            map.animateCamera(zoom);
        });
    }

    public void setAccuracy(float accuracy) {
        accuracyText.setText("Precisión: "+accuracy+"m");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @OnClick(R.id.save)
    public void save() {
        String title = titleText.getText().toString();
        String description = descriptionText.getText().toString();

        if (currentLocation == null) {
            Snackbar.make(mainView,
                    "Aun no hemos obtenido su ubicación, espere un momento porfavor.",
                    Snackbar.LENGTH_SHORT).show();
        } else if (title.isEmpty()) {
            titleText.setError(getString(R.string.error_empty_label));
            titleText.requestFocus();
        } else {
            AddressRecord record = new AddressRecord();
            record.setTitle(title);
            if (!description.isEmpty()) {
                record.setDescription(description);
            } else {
                record.setDescription(null);
            }
            record.setLatitude(currentLocation.latitude);
            record.setLongitude(currentLocation.longitude);
            new AddressesController().save(record, app.getAuth().getUid());

            Intent returnIntent = getIntent();
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }
}
