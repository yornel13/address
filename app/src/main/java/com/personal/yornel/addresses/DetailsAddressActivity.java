package com.personal.yornel.addresses;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.personal.yornel.addresses.controller.AddressesController;
import com.personal.yornel.addresses.model.AddressRecord;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsAddressActivity extends BaseActivity implements OnMapReadyCallback {

    @BindView(R.id.main_container)
    View mainView;

    @BindView(R.id.title)
    TextView titleText;

    @BindView(R.id.time)
    TextView timeText;

    @BindView(R.id.content)
    TextView contentText;

    private GoogleMap map;

    private LatLng currentLocation;
    private LatLng addressLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_address);
        ButterKnife.bind(this);
        setupMap();
        setupUI();
    }

    private void setupUI() {
        AddressRecord address = app.getAddress();
        titleText.setText(address.getTitle());
        timeText.setText(DateUtils.getRelativeTimeSpanString(address.getCreatedAt().getTime()));
        addressLocation = new LatLng(address.getLatitude(), address.getLongitude());
        if (address.getDescription() == null) {
            contentText.setVisibility(View.GONE);
        } else {
            contentText.setText(address.getDescription());
        }
    }

    @OnClick(R.id.close)
    public void close() {
        onBackPressed();
    }

    @OnClick(R.id.more)
    public void more(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        popup.getMenuInflater().inflate(R.menu.popup_details, popup.getMenu());
        popup.setOnMenuItemClickListener(item -> {
            if (item.getItemId() == R.id.menu_delete) {
                delete();
            }
            return true;
        });
        popup.show();
    }

    private void delete() {
        Dialog dialog = new AlertDialog.Builder(this, R.style.AppDialogTheme)
                .setMessage("¿Borrar dirección?")
                .setPositiveButton("Borrar", (dialog1, which) -> {
                    new AddressesController().delete(app.getAddress(), app.getAuth().getUid());
                    Intent returnIntent = getIntent();
                    setResult(RESULT_OK, returnIntent);
                    finish();
                })
                .setNegativeButton("Cancelar", null).create();
        dialog.setCanceledOnTouchOutside(true);
        Objects.requireNonNull(dialog.getWindow()).getAttributes()
                .windowAnimations = R.style.AppDialogAnimation;
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setupMap() {
        if (map == null) {
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
        map.setOnMyLocationChangeListener(location -> {
            currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
        });
        map.addMarker(new MarkerOptions()
                .position(addressLocation));
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(addressLocation, 16);
        map.animateCamera(cameraUpdate);
    }

    @OnClick(R.id.go_map)
    public void openGoogleMaps() {
        Uri uri;
        if (currentLocation == null) {
            uri = Uri.parse("http://maps.google.com/maps?" +
                    "daddr="+addressLocation.latitude+","+addressLocation.longitude);
        } else {
            uri = Uri.parse("http://maps.google.com/maps?" +
                    "saddr="+currentLocation.latitude+","+currentLocation.longitude+"&" +
                    "daddr="+addressLocation.latitude+","+addressLocation.longitude);
        }
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }
}
