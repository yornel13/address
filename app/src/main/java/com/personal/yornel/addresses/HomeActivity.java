package com.personal.yornel.addresses;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.personal.yornel.addresses.adapter.AddressAdapter;
import com.personal.yornel.addresses.controller.AddressesController;
import com.personal.yornel.addresses.event.OnAddAddressToList;
import com.personal.yornel.addresses.event.OnAddressClick;
import com.personal.yornel.addresses.event.OnRemoveAddressToList;
import com.personal.yornel.addresses.event.OnSyncAddressFailure;
import com.personal.yornel.addresses.event.OnUpdateAddressToList;
import com.personal.yornel.addresses.model.AddressRecord;
import com.personal.yornel.addresses.util.AppDividerDecoration;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends BaseActivity {

    private static final int REQUEST_CODE_CREATE = 3;
    private static final int REQUEST_CODE_DETAILS = 4;
    private static final int REQUEST_CODE_SETTINGS = 5;

    @BindView(R.id.main_container)
    View mainView;

    @BindView(R.id.empty_view)
    View emptyView;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private AddressAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setupRecyclerView();
    }

    private void setupRecyclerView() {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new AppDividerDecoration(this, DividerItemDecoration.VERTICAL), -1);
        adapter = new AddressAdapter(this, new ArrayList<>());
        recyclerView.setAdapter(adapter);
        checkList();
        new AddressesController().get(app.getAuth().getUid());
    }

    @OnClick(R.id.add)
    public void create() {
        Intent intent = new Intent(this, CreateAddressActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CREATE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_CREATE) {
            if (resultCode == RESULT_OK) {
                mainView.postDelayed(() -> Snackbar.make(mainView,
                        "Ubicación agregada con exito!",
                        Snackbar.LENGTH_LONG).show(), 500);
            }
        }
        if (requestCode == REQUEST_CODE_SETTINGS) {
            if (resultCode == RESULT_OK) {
                // do some
            }
        }
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSyncFailed(OnSyncAddressFailure event) {
        Snackbar.make(mainView, event.message, Snackbar.LENGTH_LONG).show();
        EventBus.getDefault().removeStickyEvent(OnSyncAddressFailure.class);
        checkList();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onAddToList(OnAddAddressToList event) {
        EventBus.getDefault().removeStickyEvent(OnAddAddressToList.class);
        adapter.addItemsIfNotExits(event.records);
        if (event.records.size() == 1)
            recyclerView.smoothScrollToPosition(0);
        checkList();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onRemoveFromList(OnRemoveAddressToList event) {
        EventBus.getDefault().removeStickyEvent(OnRemoveAddressToList.class);
        adapter.removeItems(event.records);
        checkList();
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUpdateList(OnUpdateAddressToList event) {
        adapter.updateItems(event.records);
        EventBus.getDefault().removeStickyEvent(OnUpdateAddressToList.class);
        checkList();
    }

    private void checkList() {
        if (adapter.getItemCount() > 0) {
            emptyView.setVisibility(View.GONE);
        } else {
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void addressClick(OnAddressClick event) {
        AddressRecord record = adapter.getItems().get(event.position);
        app.setAddress(record);

        Intent intent = new Intent(this, DetailsAddressActivity.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, event.view, Objects.requireNonNull(ViewCompat.getTransitionName(event.view)));
        ActivityCompat.startActivityForResult(this, intent, REQUEST_CODE_DETAILS, options.toBundle());
    }

    @OnClick(R.id.settings)
    public void configuration() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, REQUEST_CODE_SETTINGS);
    }
}
