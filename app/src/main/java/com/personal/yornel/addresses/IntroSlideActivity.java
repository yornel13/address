package com.personal.yornel.addresses;

import android.os.Bundle;

import com.heinrichreimersoftware.materialintro.app.IntroActivity;
import com.heinrichreimersoftware.materialintro.slide.SimpleSlide;

public class IntroSlideActivity extends IntroActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setFullscreen(true);
        super.onCreate(savedInstanceState);

        addSlide(new SimpleSlide.Builder()
                .title("¿Mala memoria?")
                .description("¿Te ha pasado que llegas a un sitio y la siguientes vez que vas no recuerdas como llegar?")
                .image(R.drawable.man_forggot)
                .background(R.color.white)
                .backgroundDark(R.color.colorPrimary)
                .scrollable(true)
                .build());
        addSlide(new SimpleSlide.Builder()
                .title("No te preocupes")
                .description("Nosotros podemos guardar las ubicaciones de esos lugares por ti de manera rapida.")
                .image(R.drawable.map_save)
                .background(R.color.white)
                .backgroundDark(R.color.colorPrimary)
                .scrollable(true)
                .build());
        addSlide(new SimpleSlide.Builder()
                .title("Facil acceso")
                .description("Puedes acceder a tus ubicaciones guardadas de manera muy facilmente con solo un toque, comencemos...")
                .image(R.drawable.man_with_phone)
                .background(R.color.white)
                .backgroundDark(R.color.colorPrimary)
                .scrollable(false)
                .build());

        setButtonNextVisible(true);
        setButtonCtaVisible(false);
        setButtonBackVisible(false);
    }
}
