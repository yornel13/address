package com.personal.yornel.addresses;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StartActivity extends BaseActivity {

    private static final int REQUEST_CODE_INTRO = 0;
    private static final int REQUEST_CODE_LOGIN = 1;
    private static final int REQUEST_CODE_HOME = 2;

    @BindView(R.id.splash_image)
    View splashImage;

    @BindView(R.id.main_container)
    View mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        checkTedPermission();
    }

    private void checkTedPermission() {
        TedPermission.with(this)
                .setPermissionListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        scheduleSplashScreen();
                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Snackbar snackbar = Snackbar.make(mainView,
                                "Si rechaza los permisos, no podras usar la aplicación",
                                Snackbar.LENGTH_INDEFINITE);
                        snackbar.setAction("Conceder", v -> checkTedPermission() );
                        snackbar.show();
                    }
                })
                .setDeniedMessage("Si rechaza los permisos, no podras usar la aplicación" +
                        "\n\nPor favor, activa los permisos en [Configuración] > [Permisos]")
                .setDeniedCloseButtonText("CERRAR")
                .setGotoSettingButtonText("CONFIGURACIÓN")
                .setPermissions(
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    private void scheduleSplashScreen() {
        splashImage.postDelayed(this::startApp, getSplashScreenDuration());
    }

    private void startApp() {
        if (preferences.isDoneIntro()) {
            FirebaseUser currentUser = app.getAuth().getCurrentUser();
            updateUser(currentUser);
        } else {
            startActivityForResult(new Intent(this, IntroSlideActivity.class),
                    REQUEST_CODE_INTRO);
        }
    }

    private void updateUser(FirebaseUser user) {
        if (user != null) {
            runApp();
        } else {
            goLogin();
        }
    }

    public void runApp() {
        Intent intent = new Intent(this, HomeActivity.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, splashImage, Objects.requireNonNull(ViewCompat.getTransitionName(splashImage)));
        ActivityCompat.startActivityForResult(this, intent, REQUEST_CODE_HOME, options.toBundle());
    }

    public void goLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, splashImage, Objects.requireNonNull(ViewCompat.getTransitionName(splashImage)));
        ActivityCompat.startActivityForResult(this, intent, REQUEST_CODE_LOGIN, options.toBundle());
    }

    protected Long getSplashScreenDuration() {
        if (preferences.isFirstLaunch()) {
            return 5000L;
        } else {
            return 1000L;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_LOGIN) {
            if (resultCode == RESULT_OK) {
                scheduleSplashScreen();
            } else {
                finish();
            }
        }
        if (requestCode == REQUEST_CODE_INTRO) {
            if (resultCode == RESULT_OK) {
                preferences.setIntroCompleted();
                scheduleSplashScreen();
            } else {
                finish();
            }
        }
        if (requestCode == REQUEST_CODE_HOME) {
            if (resultCode == RESULT_OK) {
                scheduleSplashScreen();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
    }
}
