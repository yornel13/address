package com.personal.yornel.addresses.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.personal.yornel.addresses.R;
import com.personal.yornel.addresses.event.OnAddressClick;
import com.personal.yornel.addresses.model.AddressRecord;

import org.greenrobot.eventbus.EventBus;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressAdapter extends RecyclerView.Adapter<AddressAdapter.ViewHolder> implements Filterable {

    private List<AddressRecord> records;
    private List<AddressRecord> recordsFiltered;
    private Context context;

    public AddressAdapter(Context context, ArrayList<AddressRecord> records) {
        this.records = records;
        this.recordsFiltered = records;
        this.context = context;
    }

    @Override
    public AddressAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_address, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AddressAdapter.ViewHolder viewHolder, int position) {
        if (!recordsFiltered.isEmpty()) {
            AddressRecord AddressRecord = recordsFiltered.get(position);

            viewHolder.titleText.setText(AddressRecord.getTitle());
            viewHolder.contentText.setText(DateUtils.getRelativeTimeSpanString(AddressRecord.getCreatedAt().getTime()));

//            viewHolder.cardView.setCardBackgroundColor(Color.parseColor(AddressRecord.getColor()));
//            viewHolder.cardView.setTransitionName(AddressRecord.getId());

            if (AddressRecord.getAnimate() != null && AddressRecord.getAnimate()) {
                setAnimation(viewHolder.itemView);
                AddressRecord.setAnimate(Boolean.FALSE);
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row)
        View row;

        @BindView(R.id.transition_row)
        View transitionRow;

        @BindView(R.id.title)
        TextView titleText;

        @BindView(R.id.content)
        TextView contentText;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.row)
        public void onClick() {
            EventBus.getDefault().post(new OnAddressClick(getAdapterPosition(), transitionRow));
        }
//
//        @OnLongClick(R.id.card_view)
//        public boolean onLongClick(View view) {
//            EventBus.getDefault().post(new OnNoteLongClick(getAdapterPosition(), cardView));
//            return true;
//        }

        public void clearAnimation() {
            row.clearAnimation();
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String text = charSequence.toString();
                text = normalize(text);
                if (text.isEmpty()) {
                    recordsFiltered = records;
                } else {
                    List<AddressRecord> filteredList = new ArrayList<>();
                    for (AddressRecord item : records) {
                        if(normalize(item.getTitle()).contains(text)
                                || normalize(item.getDescription()).contains(text)){
                            filteredList.add(item);
                        }
                    }
                    recordsFiltered = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = recordsFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                recordsFiltered = (ArrayList<AddressRecord>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    private void setAnimation(View viewToAnimate) {
        Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
        viewToAnimate.startAnimation(animation);
    }

    private String normalize(String input) {
        if (input == null) {
            return "";
        }
        String convertedString = Normalizer
                .normalize(input, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "").toLowerCase();
        return convertedString;
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        holder.clearAnimation();
    }

    public void notifyItemChanged(AddressRecord AddressRecord) {
        Integer position = null;
        for (AddressRecord AddressRecordF : recordsFiltered) {
            if (AddressRecord.getId().equals(AddressRecordF.getId())) {
                position = recordsFiltered.indexOf(AddressRecordF);
                break;
            }
        }
        if (position != null) {
            super.notifyItemChanged(position);
        }
    }

    public List<AddressRecord> getItems() {
        return recordsFiltered;
    }

    @Override
    public int getItemCount() {
        return recordsFiltered.size();
    }

    public void replaceAll(List<AddressRecord> list) {
        this.records = new ArrayList<>(list);
        notifyDataSetChanged();
    }

    public AddressRecord removeItem(int position) {
        final AddressRecord model = records.remove(position);
        notifyItemRemoved(position);
        return model;
    }

    public void addItems(List<AddressRecord> list) {
        this.records.addAll(list);
        notifyDataSetChanged();
    }

    public void addItem(int position, AddressRecord model) {
        records.add(position, model);
        notifyItemInserted(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final AddressRecord model = records.remove(fromPosition);
        records.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    /*
     * Agrega los items a la lista si no estan en ella
     */
    public void addItemsIfNotExits(List<AddressRecord> list) {
        List<AddressRecord> AddressRecords = new ArrayList<>(list);
        for (AddressRecord AddressRecordExternal: list) {
            for (AddressRecord AddressRecordsLocal: this.records) {
                if (AddressRecordsLocal.getId().equals(AddressRecordExternal.getId())) {
                    AddressRecords.remove(AddressRecordExternal);
                }
            }
        }
        if (AddressRecords.size() == 1) {
            this.records.add(0, AddressRecords.get(0));
            notifyItemInserted(0);
        } else {
            this.records.addAll(AddressRecords);
            notifyDataSetChanged();
        }

        Log.i("- ADAPTER ADDRESSES - ", AddressRecords.size()+" Items added");
    }

    /*
     * Remueve los items con ese id
     */
    public void removeItems(List<AddressRecord> list) {
        List<AddressRecord> AddressRecords = new ArrayList<>();
        for (AddressRecord AddressRecordExternal: list) {
            for (AddressRecord AddressRecordLocal: this.records) {
                if (AddressRecordLocal.getId().equals(AddressRecordExternal.getId())) {
                    AddressRecords.add(AddressRecordLocal);
                }
            }
        }
        for (AddressRecord AddressRecord: AddressRecords) {
            Integer index = this.records.indexOf(AddressRecord);
            this.records.remove(AddressRecord);
            notifyItemRemoved(index);
            Log.i("- ADAPTER ADDRESSES - ", "removed item id: "+AddressRecord.getId());
        }
        Log.i("- ADAPTER ADDRESSES - ", AddressRecords.size()+" Items removed");
    }

    /*
     * Cambia el estado pendiente por sincronizar a false
     */
    public void updatePending(String id) {
        for (AddressRecord AddressRecordLocal: this.records) {
            Integer index = this.records.indexOf(AddressRecordLocal);
            if (AddressRecordLocal.getId().equals(id)) {
                AddressRecordLocal.setPending(false);
                notifyItemChanged(index);
            }
        }
        Log.i("- ADAPTER NOTES - ", "Item with id '"+id+"' was updated");
    }

    /*
    * Actualiza los items con ese id
    */
    public void updateItems(List<AddressRecord> list) {
        for (AddressRecord AddressRecordExternal: list) {
            Integer index = null;
            for (AddressRecord AddressRecordtLocal: this.records) {
                if (AddressRecordtLocal.getId().equals(AddressRecordExternal.getId())) {
                    index = this.records.indexOf(AddressRecordtLocal);
                }
            }
            if (index != null) {
                this.records.set(index, AddressRecordExternal);
                notifyItemChanged(index);
            }
        }
        Log.i("- ADAPTER ADDRESSES - ", list.size()+" Items updated");
    }
}
