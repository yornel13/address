package com.personal.yornel.addresses.controller;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.ListenerRegistration;
import com.google.firebase.firestore.QuerySnapshot;
import com.personal.yornel.addresses.event.OnAddAddressToList;
import com.personal.yornel.addresses.event.OnRemoveAddressToList;
import com.personal.yornel.addresses.event.OnSaveAddressFailure;
import com.personal.yornel.addresses.event.OnSaveAddressSuccess;
import com.personal.yornel.addresses.event.OnUpdateAddressToList;
import com.personal.yornel.addresses.model.AddressRecord;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

public class AddressesController {

    public static final String USERS = "users";
    public static final String ADDRESSES = "addresses";

    private FirebaseFirestore db;

    private ListenerRegistration getListener;

    public AddressesController() {
        configureConnection();
    }

    private void configureConnection() {
        db = FirebaseFirestore.getInstance();
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder()
                .setPersistenceEnabled(true)
                .build();
        db.setFirestoreSettings(settings);
    }

    public void save(AddressRecord record, String userID) {
        final DocumentReference doc = db.collection(USERS).document(userID)
                .collection(ADDRESSES).document();
        record.setId(doc.getId());
        doc.set(record)
                .addOnSuccessListener(aVoid -> {
                    EventBus.getDefault().postSticky(new OnSaveAddressSuccess(doc.getId()));
                })
                .addOnFailureListener(e -> {
                    e.printStackTrace();
                    EventBus.getDefault().postSticky(new OnSaveAddressFailure(e.getMessage()));
                });
    }

    public void get(String userID) {
        if (getListener != null) {
            getListener.remove();
        }
        getListener = db.collection(USERS).document(userID)
                .collection(ADDRESSES)
                .addSnapshotListener((querySnapshot, e) -> {
                    if (e != null) {
                        //EventBus.getDefault().post(new OnNoteSyncFailure("Load error"));
                        return;
                    }
                    List<AddressRecord> added = new ArrayList<>();
                    List<AddressRecord> removed = new ArrayList<>();
                    List<AddressRecord> modified = new ArrayList<>();
                    for (DocumentChange change : querySnapshot.getDocumentChanges()) {
                        AddressRecord record = change.getDocument().toObject(AddressRecord.class);
                        record.setId(change.getDocument().getId());
                        record.setPending(change.getDocument().getMetadata().hasPendingWrites());
                        record.setAnimate(Boolean.TRUE);
                        if (change.getType() == DocumentChange.Type.ADDED) {
                            added.add(record);
                        } else if (change.getType() == DocumentChange.Type.REMOVED) {
                            removed.add(record);
                        } else if (change.getType() == DocumentChange.Type.MODIFIED) {
                            modified.add(record);
                        }
                    }
                    Log.i("- CONTROLLER ADDRESS -", added.size()+" items to add");
                    Log.i("- CONTROLLER ADDRESS -", removed.size()+" items to remove");
                    Log.i("- CONTROLLER ADDRESS -", modified.size()+" items to modify");

                    if (!added.isEmpty()) EventBus.getDefault().postSticky(new OnAddAddressToList(added));
                    if (!removed.isEmpty()) EventBus.getDefault().postSticky(new OnRemoveAddressToList(removed));
                    if (!modified.isEmpty()) EventBus.getDefault().postSticky(new OnUpdateAddressToList(modified));
                });
    }

    public void delete(AddressRecord address, String userID) {
        db.collection(USERS).document(userID)
                .collection(ADDRESSES).document(address.getId())
                .delete()
                .addOnSuccessListener(aVoid ->
                        Log.d("Controller", "DocumentSnapshot successfully deleted!"))
                .addOnFailureListener(e ->
                        Log.w("Controller", "Error deleting document", e));
    }
}
