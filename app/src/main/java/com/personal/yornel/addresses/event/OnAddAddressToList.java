package com.personal.yornel.addresses.event;

import com.personal.yornel.addresses.model.AddressRecord;

import java.util.List;

public class OnAddAddressToList {

    public final List<AddressRecord> records;

    public OnAddAddressToList(List<AddressRecord> records) {
        this.records = records;
    }
}
