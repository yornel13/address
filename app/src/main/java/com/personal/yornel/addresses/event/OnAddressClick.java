package com.personal.yornel.addresses.event;


import android.view.View;

public class OnAddressClick {

    public final int position;
    public final View view;

    public OnAddressClick(int position, View view) {
        this.position = position;
        this.view = view;
    }
}
