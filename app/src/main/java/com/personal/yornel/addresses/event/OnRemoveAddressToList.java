package com.personal.yornel.addresses.event;

import com.personal.yornel.addresses.model.AddressRecord;

import java.util.List;

public class OnRemoveAddressToList {

    public final List<AddressRecord> records;

    public OnRemoveAddressToList(List<AddressRecord> records) {
        this.records = records;
    }
}
