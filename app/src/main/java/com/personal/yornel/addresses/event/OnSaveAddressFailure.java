package com.personal.yornel.addresses.event;

public class OnSaveAddressFailure {

    public final String message;

    public OnSaveAddressFailure(String message) {
        this.message = message;
    }
}
