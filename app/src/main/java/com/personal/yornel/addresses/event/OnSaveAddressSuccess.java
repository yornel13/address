package com.personal.yornel.addresses.event;

public class OnSaveAddressSuccess {

    public final String id;

    public OnSaveAddressSuccess(String id) {
        this.id = id;
    }
}
