package com.personal.yornel.addresses.event;

/**
 * Created by Yornel on 29/3/2018.
 */

public class OnSyncAddressFailure {

    public final String message;

    public OnSyncAddressFailure(String message) {
        this.message = message;
    }
}
