package com.personal.yornel.addresses.event;

import com.personal.yornel.addresses.model.AddressRecord;

import java.util.List;

public class OnUpdateAddressToList {

    public final List<AddressRecord> records;

    public OnUpdateAddressToList(List<AddressRecord> records) {
        this.records = records;
    }
}
