package com.personal.yornel.addresses.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.auth.User;
import com.google.gson.Gson;

import java.util.ArrayList;

public class AppPreferences {

    private static final String PREF_FILE_NAME = "app-addresses";

    private static final String PREF_FIRST_LAUNCH = "pref_first_launch";
    public static final String PREF_DONE_INTRO = "pref_done_intro";

    private Context context;
    private SharedPreferences preferences;
    private Editor editor;

    public AppPreferences(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }

    public Boolean isFirstLaunch() {
        if (preferences.getBoolean(PREF_FIRST_LAUNCH, true)) {
            setDoneFirstLaunch();
            return true;
        } else {
            return false;
        }
    }

    public void setDoneFirstLaunch() {
        editor = preferences.edit();
        editor.putBoolean(PREF_FIRST_LAUNCH, false);
        editor.apply();
    }

    public Boolean isDoneIntro() {
        return preferences.getBoolean(PREF_DONE_INTRO, false);
    }

    public void setIntroCompleted() {
        editor = preferences.edit();
        editor.putBoolean(PREF_DONE_INTRO, true);
        editor.apply();
    }
}
